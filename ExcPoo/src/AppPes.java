import java.util.Scanner;

public class AppPes {
	
	static final int MAXNUM = 3;
	
	static int index = 0;
	
	static Pessoa[] lista = new Pessoa[MAXNUM];

	static Scanner tecla = new Scanner(System.in);
	
	static Scanner name = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int op=0;
		
		do {
			System.out.println("MENU");
			System.out.println("[1] INSERIR");
			System.out.println("[2] LISTAR");
			System.out.println("[3] SAIR");
			System.out.println("OPCAO: ");
			op = tecla.nextInt();
			
			switch (op) {
			case 1:
				incluirPessoa();
				break;
			case 2:
				listar();
				break;
			case 3:
				break;
			}
			
		} while (op!=3);
		
	}
	
	public static void incluirPessoa() {
		
		System.out.println("NOME: ");
		String n = tecla.next();
		
		lista[index++] = new Pessoa(n);
		System.out.println("PESSOA CADSTRADA COM SUCESSO!");
	}
	
	public static void listar() {
		
		System.out.println("PESSOA(s): ");
		
		for (int i = 0; i < lista.length; i++) {
			if (lista[i] != null) {
				System.out.println(lista[i].getNome());
			}
		}
	}
	
}
